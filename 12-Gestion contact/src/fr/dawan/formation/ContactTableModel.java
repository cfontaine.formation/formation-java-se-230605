package fr.dawan.formation;

import java.time.LocalDate;
import java.util.List;

import javax.swing.table.AbstractTableModel;

import fr.dawan.formation.entities.Contact;



public class ContactTableModel extends AbstractTableModel {

    private static final long serialVersionUID = 1L;

    private List<Contact> contacts;

    public ContactTableModel(List<Contact> contacts) {
        this.contacts = contacts;
    }

    public List<Contact> getContacts() {
        return contacts;
    }

    public void setContacts(List<Contact> contacts) {
        this.contacts = contacts;
    }

    @Override
    public int getRowCount() {
        return contacts.size();
    }

    @Override
    public int getColumnCount() {
        return 5;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Contact row = contacts.get(rowIndex);
        switch (columnIndex) {
        case 0:
            return row.getId();

        case 1:
            return row.getPrenom();

        case 2:
            return row.getNom();

        case 3:
            return row.getEmail();

        case 4:
            return row.getDateNaissance();

        default:
            return null;
        }
    }

    @Override
    public String getColumnName(int column) {
        switch (column) {
        case 0:
            return "Id";

        case 1:
            return "Prénom";

        case 2:
            return "Nom";

        case 3:
            return "Email";

        case 4:
            return "Date naissance";

        default:
            return null;
        }
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        switch (columnIndex) {
        case 0:
            return Long.class;
        case 4:
            return LocalDate.class;
        default:
            return String.class;
        }
    }

}
