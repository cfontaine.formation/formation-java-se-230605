package fr.dawan.formation;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import fr.dawan.formation.dao.ContactDao;
import fr.dawan.formation.entities.Contact;


// Le projet 12-gestion contact dépent du projet 09-jdbc (on utilise Contact et ContactDAO)
// Pour ajouter cette dépendence au projet
// Clique droit sur le projet -> properties -> Java Build Path
// onglet projet -> sur classpath -> add ... -> on coche 09-jdbc

public class Application {

    private JFrame frame;
    private JTable table;
    private ContactTableModel model;
    private ContactDao dao;

    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    Application window = new Application();
                    window.frame.setVisible(true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public Application() {
        dao = new ContactDao();
        initialize();
    }

    private void initialize() {
        // Initialisation de la fenêtre
        frame = new JFrame("Gestion Contact");
        frame.setBounds(100, 100, 533, 381);
        frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        //
        frame.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                closeApplication();
            }
        });

        // Initialisation des boutons
        JPanel panelBtn = new JPanel();
        FlowLayout flowLayout = (FlowLayout) panelBtn.getLayout();
        flowLayout.setHgap(20);
        frame.getContentPane().add(panelBtn, BorderLayout.SOUTH);

        JButton btnAjouter = initButtonAjouter();
        panelBtn.add(btnAjouter);

        JButton btnModifier = initButtonModifier();
        panelBtn.add(btnModifier);

        JButton btnSupprimer = initButtonSupprimer();
        panelBtn.add(btnSupprimer);

        JButton btnQuitter = initButtonQuitter();
        panelBtn.add(btnQuitter);

        // Initialisation de la JTable
        JScrollPane scrollPane = new JScrollPane();
        frame.getContentPane().add(scrollPane, BorderLayout.CENTER);

        table = new JTable();
        table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        scrollPane.setViewportView(table);

        try {
            model = new ContactTableModel(dao.findAll());
            table.setModel(model);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(frame, e.getMessage(), "Initalisation de la JTable",
                    JOptionPane.ERROR_MESSAGE);
            e.printStackTrace();
        } finally {
            dao.close();
        }

        // Activation des boutons supprimer et modifier, si un contact est sélectionné
        table.getSelectionModel().addListSelectionListener(new ListSelectionListener() {

            @Override
            public void valueChanged(ListSelectionEvent e) {
                boolean selected = table.getSelectedRow() >= 0; // -1 si aucun contact sélectionné
                btnSupprimer.setEnabled(selected);
                btnModifier.setEnabled(selected);
            }
        });
    }

    private JButton initButtonAjouter() {
        JButton btn = new JButton("Ajouter");
        btn.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                ContactDialog d = new ContactDialog("Ajouter un contact");
                Contact c = d.show(frame);
                d.dispose();
                if (c != null) {
                    try {
                        dao.saveOrUpdate(c);
                        model.setContacts(dao.findAll());
                        model.fireTableDataChanged();
                    } catch (Exception e1) {
                        JOptionPane.showMessageDialog(frame, e1.getMessage(), "Ajouter un contact",
                                JOptionPane.ERROR_MESSAGE);
                        e1.printStackTrace();
                    } finally {
                        dao.close();
                    }
                }
            }
        });
        return btn;
    }

    private JButton initButtonModifier() {
        JButton btn = new JButton("Modifier");
        btn.setEnabled(false);
        btn.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                try {
                    Long id = (Long) table.getModel().getValueAt(table.getSelectedRow(), 0);
                    Contact c = dao.findById(id);
                    ContactDialog d = new ContactDialog("Modifier un contact", c);
                    c = d.show(frame);
                    d.dispose();
                    if (c != null) {
                        dao.saveOrUpdate(c);
                        model.setContacts(dao.findAll());
                        model.fireTableDataChanged();
                    }
                } catch (Exception e1) {
                    JOptionPane.showMessageDialog(frame, e1.getMessage(), "Modifer un contact",
                            JOptionPane.ERROR_MESSAGE);
                    e1.printStackTrace();
                } finally {
                    dao.close();
                }
            }
        });
        return btn;
    }

    private JButton initButtonSupprimer() {
        JButton btn = new JButton("Supprimer");
        btn.setEnabled(false);
        btn.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                try {
                    List<Contact> contacts = dao.findAll();
                    Contact contactSup = contacts.remove(table.getSelectedRow());
                    int choix = JOptionPane.showConfirmDialog(frame,
                            "Voulez vous supprimer " + contactSup.getPrenom() + " " + contactSup.getNom(), "Supprimer",
                            JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
                    if (choix == JOptionPane.YES_OPTION) {
                        dao.remove(contactSup);
                        model.setContacts(contacts);
                        model.fireTableDataChanged();
                    }

                } catch (Exception e1) {
                    JOptionPane.showMessageDialog(frame, e1.getMessage(), "Supprimer un contact",
                            JOptionPane.ERROR_MESSAGE);
                    e1.printStackTrace();
                } finally {
                    dao.close();
                }
            }
        });
        return btn;
    }

    private void closeApplication() {
        int choix = JOptionPane.showConfirmDialog(frame, "Voulez quitter l'application?", "Gestion contact",
                JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
        if (choix == JOptionPane.YES_OPTION) {
            frame.setVisible(false);
            frame.dispose();
            System.exit(0);
        }
    }

    private JButton initButtonQuitter() {
        JButton btn = new JButton("Quitter");
        btn.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                closeApplication();
            }
        });
        return btn;
    }

}
