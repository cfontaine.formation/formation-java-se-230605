
package fr.dawan.formation;

public class Application {

    public static void main(String[] args) {
        // Classe Générique
        Pair<Integer, String> p1 = new Pair<>(42, "Bonjour");
        System.out.println(p1);

        Pair<Double, Character> p2 = new Pair<>(4.6, 'a');
        System.out.println(p2);

        // Méthode Générique
        // boolean tst1=Application.<String>egalite("azerty", "42");
        // Le type générique est inféré à partir du type des paramètres
        boolean tst1 = Application.egalite("azerty", "42");
        System.out.println(tst1);
        tst1 = egalite(5, 5);
        System.out.println(tst1);
    }

    // Méthode Générique
    public static <T> boolean egalite(T a, T b) {
        return a.equals(b);
    }
}
