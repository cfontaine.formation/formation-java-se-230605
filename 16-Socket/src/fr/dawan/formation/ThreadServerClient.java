package fr.dawan.formation;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Vector;

public class ThreadServerClient extends Thread {

    private static Vector<ThreadServerClient> clientThreads = new Vector<>();

    private Socket s;

    public ThreadServerClient(Socket socket) {
        s = socket;
        clientThreads.add(this);
    }

    @Override
    public void run() {
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(s.getInputStream()));
                PrintWriter writer = new PrintWriter(s.getOutputStream(), true);) {
            String message = "";
            while (!message.equals("quit")) {
                // lecture du flux
                message = reader.readLine();
                // renvoie de l'écho du message
                writer.println("écho: " + message);
                System.out.println("message=" + message);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        clientThreads.remove(this);
    }

    public static Vector<ThreadServerClient> getClientSocket() {
        return clientThreads;
    }
}
