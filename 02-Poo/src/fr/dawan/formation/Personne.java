package fr.dawan.formation;

import fr.dawan.formation.beans.Adresse;

public class Personne {
    
    String prenom;
    
    String nom;
    
    String  email;
    
    Adresse adresse; // agregation

    public Personne(String prenom, String nom, Adresse adresse) {
        this.prenom = prenom;
        this.nom = nom;
        this.adresse = adresse;
    }

    public Personne(String prenom, String nom, String email, Adresse adresse) {
        this(prenom,nom,adresse);
        this.adresse = adresse;
    }

    public void afficher(){
        System.out.print(prenom + " " + nom+ " ");
        if(email !=null ) {
            System.out.print( email + " ");
        }
        adresse.afficher();
    }
    
}
