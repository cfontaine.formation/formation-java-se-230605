package fr.dawan.formation;

public class VoiturePrioritaire extends Voiture {

    private boolean gyro;
    
    public VoiturePrioritaire() {
        // super(); // Appel implicite du constructeur par défaut de la classe mère, s'il existe
        System.out.println("Constructeur par défaut voiture prioritaire");
    }

    public VoiturePrioritaire(String marque, String couleur, String plaqueIma,boolean gyro) {
        super(marque, couleur, plaqueIma); // Appel explicite du constructeur 3 paramètres de la classe mère 
        System.out.println("Constructeur 3 paramètres voiture prioritaire");
        this.gyro=gyro;
    }

    public void alummerGyro() {
        gyro=true;
    }
    
    public void eteindreGyro() {
        gyro=false;
    }

    public boolean isGyro() {
        return gyro;
    }
    
    // Redéfinition de la méthode afficher de Voiture
    @Override
    public void afficher() {
        super.afficher();   // -> appel de la méthode afficher() de la classe Voiture 
        System.out.println(gyro?"gyro allumé":" gyro eteint" );
    }

}
