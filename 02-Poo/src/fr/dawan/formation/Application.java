package fr.dawan.formation;

import java.util.Date;

// Comme la classe Adresse n'est pas dans le même package que la classe Application 
// Il faut indiquer avec import dans quel package se trouve la classe Adresse
import fr.dawan.formation.beans.Adresse;

import static java.lang.Math.min;

public class Application {

    public static void main(String[] args) {
        // Appel d'une méthode de classe
        Voiture.testMethodeClasse();
        System.out.println("Nombre voiture=" + Voiture.getNbVoiture()); // 0 //Voiture.nbVoiture

        // Instantiation de la classe Voiture
        Voiture v1 = new Voiture();
        System.out.println("Nombre voiture=" + Voiture.getNbVoiture()); // 1
        v1.afficher();
        // v1.vitesse = 20; // n'est plus accessible => privé
        // Encapsulation => Pour accéder aux attributs, on doit passer par les méthodes get
        System.out.println(v1.getVitesse()); // v1.vitesse

        // Appel d'une méthode d'instance
        v1.accelerer(15);
        System.out.println(v1.getVitesse());
        v1.freiner(10);
        v1.afficher();
        System.out.println(v1.estArreter());
        v1.arreter();
        // v1.afficher();
        System.out.println(v1.estArreter());

        Voiture v2 = new Voiture();
        System.out.println("Nombre voiture=" + Voiture.getNbVoiture()); // 2
        // v2.vitesse = 10;
        System.out.println(v2.getVitesse());

        Voiture v3 = new Voiture("Fiat", "Jaune", "AZ-123-FR");
        System.out.println("Nombre voiture=" + Voiture.getNbVoiture()); // 3

        // Méthode de classe
        System.out.println(Voiture.egaliteVitesse(v2, v3));

        v3 = null; // comme il n'est plus accéssible l'objet est candidat à la destruction par le garbage collector
        // System.gc(); // Appel explicite au Garbage collector => à eviter

        // Agrégation
        Adresse adr=new Adresse("46 rue des cannoniers", "Lille", "59000");
        Personne per1 = new Personne("John", "Doe",adr );
        Voiture v4 = new Voiture("Honda", "Rouge", "er-4567-FR", per1);
        v4.afficher();

        // Exemple import
        Date dateJour;          // Quand on utilise Date, c'est la classe Date du package java.utils qui est utilisé (import) 
        java.sql.Date dateSql;  // Si on veut utiliser la classe Date du package java.sql, il faut donner le nom complet de la classe package + nom classe
                                // On ne peut pas utiliser import : conflit avec java.utils.Date 
        // import static => on n'a plus besoin de préciser le nom de la classe, quand utilise la méthode min
        System.out.println(min(1,4));

        // Héritage
        VoiturePrioritaire vp1 = new VoiturePrioritaire();
        vp1.accelerer(10);
        vp1.afficher();
        vp1.alummerGyro();
        System.out.println(vp1.isGyro());
        vp1.afficher();
        
        // final sur un attribut => constante
        System.out.println(Voiture.VITESSE_MAX);

        // Exercice: Compte Bancaire
        CompteBancaire cb1 = new CompteBancaire(200.0, "John Doe");
        // cb1.solde = 200.0;
        // cb1.iban = "fr-625900000";
        // cb1.titulaire = "John Doe";

        cb1.afficher();
        cb1.crediter(100.0);
        cb1.afficher();
        cb1.debiter(400.0);
        cb1.afficher();
        System.out.println(cb1.estPositif());

        // Exercice Héritage
        CompteEpargne ce1 = new CompteEpargne(3000, "Jane Doe", 1.2);
        ce1.afficher();
        ce1.calculInteret();
        ce1.afficher();

        // Exercice Dé
        De d1 = new De(6);
        De d2 = new De(6);
        System.out.println(De.compValeur(d1, d2)); // true
        d1.setValeur(3);
        d1.setBloque(true);
        d1.lancer();
        d2.lancer();
        d1.afficher();
        d2.afficher();
        System.out.println(De.compValeur(d1, d2));
    }

}
