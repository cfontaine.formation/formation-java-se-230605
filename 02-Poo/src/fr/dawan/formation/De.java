package fr.dawan.formation;

public class De {
    private int nombreFace;
    private int valeur = 1;
    private boolean bloque;

    public De(int nombreFace) {
        this.nombreFace = nombreFace;
    }

    public De(int nombreFace, int valeur) {
        this(nombreFace);
        setValeur(valeur);
    }

    public int getValeur() {
        return valeur;
    }

    public void setValeur(int valeur) {
        if (valeur <= nombreFace && valeur > 0) {
            this.valeur = valeur;
        }
        // else{
        // Lancer une exception
        // }
    }

    public boolean isBloque() {
        return bloque;
    }

    public void setBloque(boolean bloque) {
        this.bloque = bloque;
    }

    public int getNombreFace() {
        return nombreFace;
    }

    public void lancer() {
        if (!bloque) {
            valeur = 1 + (int) (Math.random() * nombreFace);
        }
    }

    public void afficher() {
        System.out.println(valeur);
    }

    public static boolean compValeur(De d1, De d2) {
        return d1.valeur == d2.valeur;
    }
}
