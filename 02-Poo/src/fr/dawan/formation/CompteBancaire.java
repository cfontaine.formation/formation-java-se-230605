package fr.dawan.formation;

public class CompteBancaire {

    protected double solde = 50.0;
    private String iban;
    private String titulaire;

    private static int cptCompte;

    public CompteBancaire(String titulaire) {
        this.titulaire = titulaire;
        cptCompte++;
        iban = "fr5900-" + cptCompte;
    }

    public CompteBancaire(double solde, String titulaire) {
        this(titulaire);
        this.solde = solde;
    }

    public String getTitulaire() {
        return titulaire;
    }

    public void setTitulaire(String titulaire) {
        this.titulaire = titulaire;
    }

    public double getSolde() {
        return solde;
    }

    public String getIban() {
        return iban;
    }

    public void afficher() {
        System.out.println("----------------");
        System.out.println("solde=" + solde);
        System.out.println("iban=" + iban);
        System.out.println("titulaire=" + titulaire);
        System.out.println("----------------");
    }

    public void crediter(double valeur) {
        if (valeur > 0) {
            solde += valeur;
        }
    }

    public void debiter(double valeur) {
        if (valeur > 0) {
            solde -= valeur;
        }
    }

    public boolean estPositif() {
        return solde >= 0;
    }
}
