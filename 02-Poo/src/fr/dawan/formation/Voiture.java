package fr.dawan.formation;

//final  => interdire l'héritage
public /*final*/ class Voiture {

    // final -> constante
    public static final int VITESSE_MAX=250;
    
    // Variable d'instance (attribut)
    private String marque;
    private String couleur = "rouge";
    private String plaqueIma;
    protected int vitesse;  // protected -> les classes enfants ont accès à cette variable d'instance 
    private int compteurKm = 200;
    
    // Agrégation
    private Personne proprietaire;
    
    // Moteur
    private Moteur moteur; // Composition

    // Variable de classe
    private static int nbVoiture;
    
    // Constructeurs
    // Avec eclipse, on peut générer les constructeurs
    // Menu contextuel -> Source -> Generarted constructror using fields...

    // Constructeur par défaut (sans paramètres)
    public Voiture() {
        System.out.println("Constructeur par défaut");
        moteur=new Moteur();
        nbVoiture++;
    }

    // On peut surcharger le constructeur
    public Voiture(String marque, String couleur, String plaqueIma) {
        this(); // Chainage des constructeurs
        System.out.println("Constructeur 3 paramètres");
        this.marque = marque; // utilisation de this pour indiquer que l'on acccède à la variable d'instance
        this.couleur = couleur;
        this.plaqueIma = plaqueIma;
    }

    public Voiture(String marque, String couleur, String plaqueIma, int vitesse, int compteurKm) {
//        this.marque = marque;
//        this.couleur = couleur;
//        this.plaqueIma = plaqueIma;
        this(marque, couleur, plaqueIma); // Chainage des constructeurs
        System.out.println("Constructeur 5 paramètres");
        this.vitesse = vitesse;
        this.compteurKm = compteurKm;
    }
    
    public Voiture(String marque, String couleur, String plaqueIma, Personne proprietaire) {
        this(marque, couleur, plaqueIma);
        this.proprietaire = proprietaire;
    }
    
    // getter/setter
    public int getVitesse() {
        return vitesse;
    }
    
    public String getCouleur() {
        return couleur;
    }
    
    public void setCouleur(String couleur) {
        if(couleur!=null) {
            this.couleur=couleur;
        }
    }
    
    public Personne getProprietaire() {
        return proprietaire;
    }

    public void setProprietaire(Personne proprietaire) {
        this.proprietaire = proprietaire;
    }

    public String getMarque() {
        return marque;
    }

    public String getPlaqueIma() {
        return plaqueIma;
    }

    public int getCompteurKm() {
        return compteurKm;
    }

    public static int getNbVoiture() {
        return nbVoiture;
    }

    // Méthode d'instance
    public void accelerer(int vAcc) {
        if (vAcc > 0) {
            vitesse += vAcc;
        }
    }

    public void freiner(int vFrn) {
        if (vFrn > 0) {
            vitesse -= vFrn;
        }
    }

    public void arreter() {
        vitesse = 0;
    }

    public boolean estArreter() {
        return vitesse == 0;
    }

    // final => interdit la redéfinition de la méthode
    public /*final*/ void afficher() {
        System.out.println(marque + " " + couleur + " " + plaqueIma + " " + vitesse + " " + compteurKm);
    }

    // Méthode de classe
    public static void testMethodeClasse() {
        System.out.println("Méthode de classe");
        // Les méthodes et les variables d'instances ne peuvent pas être utilisées  directement dans une méthode de classe
        // System.out.println(vitesse);
        // arreter();
        
        // Mais on peut accéder aux variables et aux autres méthodes de classe 
        System.out.println(nbVoiture);
    }

    // Dans un méthode de classe on peut accèder à une variable d'instance
    // si la référence d'un objet est passée en paramètre
    public static boolean egaliteVitesse(Voiture va, Voiture vb) {
        if (va.vitesse == vb.vitesse) {
            return true;
        } else {
            return false;
        }
    }
}
