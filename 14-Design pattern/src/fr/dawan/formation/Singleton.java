package fr.dawan.formation;


public class Singleton {
    
    private int data;
    
    private static Singleton instance;

    public static Singleton getInstance() {
        if(instance==null) {
            instance =new Singleton();
        }
        return instance;
    }

    private Singleton() {
        
    }
    
    public int getData() {
        return data;
    }

    public void setData(int data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "Singleton [data=" + data + ", toString()=" + super.toString() + "]";
    }
    
}

