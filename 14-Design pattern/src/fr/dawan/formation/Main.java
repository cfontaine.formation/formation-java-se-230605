package fr.dawan.formation;


public class Main {

    public static void main(String[] args) {
        Singleton a=Singleton.getInstance();
        a.setData(12);
        System.out.println(a);
        
        // ...
        Singleton b=Singleton.getInstance();
        System.out.println(b);
        b.setData(42);
        
        // ...
        Singleton c=Singleton.getInstance();
        System.out.println(c);
        
        // Singleton d=new Singleton();
    }

}
