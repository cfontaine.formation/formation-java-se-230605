package fr.dawan.formation;

import fr.dawan.formation.exceptions.UnknownOperatorException;

public class Calculatrice {
    
    public static void calcul(double v1,double v2,String op) throws ArithmeticException,UnknownOperatorException {
        switch (op) {
        case "+":
            System.out.println(v1 + " + " + v2 + " =" + (v1 + v2));
            break;
        case "-":
            System.out.println(v1 + " - " + v2 + " =" + (v1 - v2));
            break;
        case "x":
            System.out.println(v1 + " x " + v2 + " =" + (v1 * v2));
            break;
        case "/":
            if (v2 == 0.0) { 
                throw new ArithmeticException("Division par 0");
            } else {
                System.out.println(v1 + " / " + v2 + " =" + (v1 / v2));
            }
            break;
        default:
            throw new UnknownOperatorException("L'opérateur " + op + " n'existe pas");
        }

    }

}
