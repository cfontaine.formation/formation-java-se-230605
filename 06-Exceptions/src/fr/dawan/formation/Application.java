package fr.dawan.formation;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Scanner;

import fr.dawan.formation.exceptions.AgeNegatifException;
import fr.dawan.formation.exceptions.EmployeException;

public class Application {

    public static void main(String[] args) {
        // Checked exception => on est obligé (par java) de traiter l'exception
        FileInputStream fi = null;
        try {
            // ...
            // ouverture d'un fichier qui n'existe pas => FileInputStream lance une
            // exception FileNotFoundException
            fi = new FileInputStream("nexistepas");
            int a = fi.read(); // read peut lancer une IOException
            System.out.println("Suite du code");
            // pour un bloc try, il peut y avoir plusieurs blocs catch pour traiter
            // diférentes exceptions de la + spécifique à la + générale

        } catch (FileNotFoundException e) { // Attrape une exception FileNotFoundException
            System.err.println("Le fichier n'existe pas");
            System.err.println(e.getMessage());
            e.printStackTrace();

        } catch (IOException e) {
            System.err.println("Impossible de lire le fichier");

        } catch (Exception e) { // attrape tous les exceptions autre que FileNotFoundException et IOException
            System.err.println("Une autre exception");

        } finally {
            System.out.println("Toujours executé");
            if (fi != null) {
                try {
                    fi.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        // Runtime Exception => on peut traiter l'exception ou pas (pas d'obligation)
        try {
            int[] tab = new int[5];
            tab[40] = 23; // => ArrayIndexOutOfBoundsException
            System.out.println("Suite du code");
        } catch (ArrayIndexOutOfBoundsException e) {
            e.printStackTrace();
        }

        Scanner sc = new Scanner(System.in);
        int age = sc.nextInt();
        try {
            traitementEmploye(age);
        } catch (EmployeException e) {
            System.err.println(e.getMessage());
            e.printStackTrace();
        }
        System.out.println("Suite du programme");
        sc.close();
    }

    public static void traitementEmploye(int age) throws EmployeException {
        System.out.println("Debut traitement employe");
        try {
            traitementAge(age);
        } catch (AgeNegatifException e) {
            // traitement local
            System.err.println("traitement partiel de l'exception");
            // throw e; // Relancer une exception
            throw new EmployeException("Employe age négatif", e); // Relancer une exception d'un autre type
        }
        System.out.println("Fin traitement employe");
    }

    public static void traitementAge(int age) throws AgeNegatifException {
        System.out.println("Debut traitement age");
        if (age < 0) {
            throw new AgeNegatifException(age); // Lancer une exception pour signaler une erreur
                                                // L'exception va remonter la pile d'appel des méthodes
                                                // tant qu'elle n'est pas traiter
        }
        System.out.println("Fin traitement age");
    }

}
