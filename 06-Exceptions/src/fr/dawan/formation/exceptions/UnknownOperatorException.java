package fr.dawan.formation.exceptions;

public class UnknownOperatorException extends Exception {

    private static final long serialVersionUID = 1L;

    public UnknownOperatorException() {
        super();

    }

    public UnknownOperatorException(String message, Throwable cause) {
        super(message, cause);
        }

    public UnknownOperatorException(String message) {
        super(message);
    }
}
