package fr.dawan.formation.exceptions;

//Créer ses propres exceptions => hériter de la classe Exception ou une de ses sous-classses
public class AgeNegatifException extends Exception {

    private static final long serialVersionUID = 1L;

    public AgeNegatifException() {
        super();
    }

    public AgeNegatifException(int age) {
        super("L'age :" + age + " est négatif");
    }

}
