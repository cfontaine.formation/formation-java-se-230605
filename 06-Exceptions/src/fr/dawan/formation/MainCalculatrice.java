package fr.dawan.formation;

import java.util.InputMismatchException;
import java.util.Scanner;

import fr.dawan.formation.exceptions.UnknownOperatorException;

public class MainCalculatrice {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

            System.out.print("Saisir votre opération:");
            try {
                double v1 = sc.nextDouble();
                String op = sc.next();
                double v2 = sc.nextDouble();
                Calculatrice.calcul(v1, v2, op);
            }
            catch (UnknownOperatorException e) {
                System.err.println(e.getMessage());
            } catch (InputMismatchException e) {
                System.err.println("La saisie des nombres est incorrecte");
            }
            catch (ArithmeticException e) {
                System.err.println(e.getMessage());
            }
        sc.close();
    }

}
