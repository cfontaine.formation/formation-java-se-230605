package fr.dawan.formation.enums;

public enum TypeContrat {
    CDD, CDI, INTERIM, STAGE
}
