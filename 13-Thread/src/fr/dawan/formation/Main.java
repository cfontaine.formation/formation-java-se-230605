package fr.dawan.formation;

public class Main {

    public static void main(String[] args) {
        System.out.println(Thread.currentThread().getName());

        MyRunnable tr = new MyRunnable();
        Thread t1 = new Thread(tr);
        t1.setName("T1");
        t1.setPriority(Thread.MIN_PRIORITY);

        MyThread t2 = new MyThread();
        t2.setName("T2");
        t2.setPriority(Thread.MAX_PRIORITY);
        // t2.setDaemon(true);

        MyThread t3 = new MyThread();
        t3.setName("T3");
        // t3.setDaemon(true);

        t1.start();
        try {
            t1.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        t2.start();
        t3.start();

        for (int i = 0; i < 50; i++) {
            System.out.println("main=" + i);
        }
        t2.interrupt();
    }

}
