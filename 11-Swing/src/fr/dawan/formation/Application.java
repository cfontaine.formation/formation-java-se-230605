package fr.dawan.formation;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.DisplayMode;
import java.awt.GraphicsEnvironment;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.UIManager.LookAndFeelInfo;

public class Application {

    public static void main(String[] args) {
        // Look and feel
        // -----------------------------------------------------------
        // Afficher le nom des classes des Look and feel installés
        LookAndFeelInfo[] lfi = UIManager.getInstalledLookAndFeels();
        for (LookAndFeelInfo li : lfi) {
            System.out.println(li.getClassName());
        }

        // Look and feel du système
////      String laf=UIManager.getSystemLookAndFeelClassName();

        // Look and feel du cross plateforme par défaut
//        String laf=UIManager.getCrossPlatformLookAndFeelClassName();
//        try {
//            UIManager.setLookAndFeel(laf);
//        } catch (ClassNotFoundException e) {
//            e.printStackTrace();
//        } catch (InstantiationException e) {
//            e.printStackTrace();
//        } catch (IllegalAccessException e) {
//            e.printStackTrace();
//        } catch (UnsupportedLookAndFeelException e) {
//            e.printStackTrace();
//        }
        // -----------------------------------------------------------
        // Obtenir les dimensions de l'écran (ne fonctionneme pas toujours)
        Dimension size = Toolkit.getDefaultToolkit().getScreenSize();
        System.out.println(size);

        // autre moyen d'obtenir les dimensions de l'écran
        DisplayMode mode = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice().getDisplayMode();
        System.out.println(mode.getWidth() + " x " + mode.getHeight());
        // -----------------------------------------------------------
       
        // Création d'une fenêtre (conteneur)
        JFrame frame = new JFrame("Exemple Swing");

        // Définir les dimensions de la fenêtre
        frame.setSize(800, 600);

        // Définir une dimmension minimum pour fenêtre
        frame.setMinimumSize(new Dimension(300, 220));

        // Définir la position de la fenêtre
        frame.setLocation(100, 100);

        // Configurer le comportement de la fenêtre à la fermeture
        // Arrét du programme lorsque l'on ferme la fenêtre
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        // Pour empécher le redimensionnement de la fenêtre
        // frame.setResizable(false);

        // Création d'un bouton (composant)
        JButton bp1 = new JButton("Ok");
        JButton bp2 = new JButton("Not Ok");
        bp1.setPreferredSize(new Dimension(120, 50));
        bp2.setPreferredSize(new Dimension(150, 50));

        // Layout Manager -> positionnement des composants

        // Positionnement Absolue
//       frame.setLayout(null);
//       bp1.setBounds(10,10,150,50);
//       bp2.setBounds(450,300, 200,40);

        // FlowLayout
//         frame.setLayout(new FlowLayout(FlowLayout.LEFT,20,50));
//      //  Ajout des boutons au contentPane de la fenêtre
//         frame.getContentPane().add(bp1);
//         frame.getContentPane().add(bp2);

        // BorderLayout
//        frame.setLayout(new BorderLayout());
//        frame.getContentPane().add(bp1, BorderLayout.CENTER);
//        frame.getContentPane().add(bp2, BorderLayout.PAGE_END);

        // GridLayout
//        frame.setLayout(new GridLayout(5, 4, 30, 10));
//        for (int i = 0; i < 20; i++) {
//            frame.getContentPane().add(new JButton("Bouton " + i));
//        }

        // Boxlayout
//        frame.setLayout(new BoxLayout(frame.getContentPane(), BoxLayout.X_AXIS));
//        frame.getContentPane().add(bp1);
//        frame.getContentPane().add(bp2);

        // JPanel -> panneau (conteneur)
        JPanel pan1 = new JPanel();
        pan1.add(bp1);
        pan1.add(bp2);
        frame.getContentPane().add(pan1, BorderLayout.PAGE_END);

        JPanel pan2 = new JPanel();
        JLabel label = new JLabel();
        label.setText("text label");
        JTextField tf1 = new JTextField("init", 15);
        pan2.add(label);
        pan2.add(tf1);
        frame.getContentPane().add(pan2, BorderLayout.CENTER);

        // ActionListener
        // 1- Classe annonyme
        bp1.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                int choix = JOptionPane.showConfirmDialog(frame, "Afficher une autre boite de dialogue", "Question",
                        JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
                if (choix == JOptionPane.YES_OPTION) {
                    JOptionPane.showMessageDialog(frame, "Message de la boite de dialogue",
                            "Le titre de la boite de dialogue", JOptionPane.INFORMATION_MESSAGE);
                }
                String reponse = JOptionPane.showInputDialog(frame, "Entrer un chaine de caractère", "init value");
                System.out.println(reponse);
            }
        });

        // 2 Classe qui implémente l'interface ActionListener
        BoutonAction ba = new BoutonAction();
        bp1.addActionListener(ba);
        // setActionCommand -> permet d'associé une commande (chaine de caractères) à un composant
        bp1.setActionCommand("bp1");
        bp2.addActionListener(ba);
        bp2.setActionCommand("bp2");

        // MouseListener
        // MouseAdapter -> classe abstraite qui implémente l'interface MouseListener
        // avec des méthodes vides
        // cela permet de ne redéfinir que les méthodes nécessaires
        bp1.addMouseListener(new MouseAdapter() {

            @Override
            public void mouseEntered(MouseEvent e) {
                System.out.println(e.getX() + "  " + e.getXOnScreen());
            }
        });

        // KeyListener
        bp1.addKeyListener(new KeyAdapter() {

            @Override
            public void keyPressed(KeyEvent e) {
                System.out.println(e.getKeyCode() + " " + e.getKeyChar());
            }
        });

        // pack -> Donne à la fenêtre, la taille nécessaire pour respecter la
        // preferredSize des composants
        // frame.pack();

        // Rendre la fenêtre visible
        frame.setVisible(true);
    }
}


