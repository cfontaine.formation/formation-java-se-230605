package fr.dawan.formation;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class BoutonAction implements ActionListener {

    @Override
    public void actionPerformed(ActionEvent e) {
        switch (e.getActionCommand()) { // getActionCommand() de ActionEvent permet d'obtenir la commande associé au
                                        // composant 
        case "bp1":
            System.out.println("Action Bouton1");
            break;
        case "bp2":
            System.out.println("Action Bouton2");
            break;
        }
    }

}

