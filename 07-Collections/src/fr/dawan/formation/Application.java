package fr.dawan.formation;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Queue;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

import fr.dawan.formation.beans.Employe;
import fr.dawan.formation.beans.Entreprise;
import fr.dawan.formation.beans.User;
import fr.dawan.formation.enums.TypeContrat;

public class Application {

    public static void main(String[] args) {
        // Avant java SE 5 => on pouvait stocker tous les objets qui héritent de Object dans une collection
        List lst1 = new ArrayList();    // List => interface , ArrayList =>objet "réel" qui implémente l'interface
        lst1.add("Hello");
        lst1.add(12);   // autoboxing convertion automatique type primtif -> objet de type enveloppe
                        // implicitement new Integer(12)
        lst1.add(3.5);

        // get => récupérer un objet stocké dans la liste à l'indice 0
        // retourne des objects, il faut tester si l'objet correspont à la classe et le caster
        if (lst1.get(0) instanceof String) {
            String str = (String) lst1.get(0);
            System.out.println(str);
        }

        // parcourir avec un itérateur
        Iterator it = lst1.iterator();
        while (it.hasNext()) {
            System.out.println(it.next());
        }

        // ------------------------------------
        // Exemple de classe générique
        Box<String> b1 = new Box<>("azerty");
        System.out.println(b1.getValeur());

        // Type Wrapper
        // Autoboxing conversion automatique d'un type primitif vers un wraper
        Integer iWa = 12; // int -> Integer: 12 est convertie automatiquement vers Integer(12) 
        Box<Integer> b2 = new Box<>(12); 
        
        // UnBoxing conversion automatique d'un wraper vers un type primitif
        int i = iWa; // Integer -> int
        int a = b2.getValeur();

        // Convertion String -> type primitif
        double d = Double.parseDouble("12.3");
        
        // Convertion String -> type wrapper
        Double dWa = Double.valueOf("12.3");
        System.out.println(iWa);

        // Convertion int -> String
        String strI = Integer.toString(12);
        String strIHexa = Integer.toHexString(42);   // en hexa
        String strIOct = Integer.toOctalString(42);  // en octal
        String strIBin = Integer.toBinaryString(42); // en binaire
        // ------------------------------------

        // à partir de java 5 => les collections utilisent les types générique
        List<String> lstStr = new ArrayList<>();    // la liste ne peut plus contenir que des chaines de caractères
        lstStr.add("Hello");
        // lstStr.add(12); // -> Erreur 
        lstStr.add("azerty");
        lstStr.add("Bonjour");
        lstStr.add("asupprimer");

        // size => le nombre d'élément de la collection
        System.out.println(lstStr.size()); // 4

        //-------------------------------------
        // Uniquement pour les list -> index
        
        // get => revoie l'élément placé à l'index 1
        System.out.println(lstStr.get(1));
        
        // set => remplace l'élément à l'index 1 par celui passé en paramètre
        System.out.println(lstStr.set(1, "World")); // set retourne azerty
        
        // Suppprimer l'élement à l'index 3
        lstStr.remove(3);
        //-------------------------------------
        
        // pour toutes ls collecton on peut aussi supprimer un élément de la liste ele passant en paramètre à remmove
        // lstStr.remove("asuprrimer"); // supprime le premier élément "asupprimer"

        // Parcourir une collection "foreach" (java5)
        for (String elm : lstStr) {
            System.out.println(elm);
        }
        
        // On peut afficher le contenu d'un collection avec System.out.println()
        System.out.println(lstStr);

        // Set => pas de doublons
        Set<Integer> st1 = new HashSet<>();
        st1.add(1);
        st1.add(21);
        System.out.println(st1.add(5));  // 5 est ajouté add retourne true
        System.out.println(st1.add(21)); // doublons -> 21 existe déjà dans la liste
                                         // il n'est pas ajouté et add retourne false
        System.out.println(st1.size());  // 3
        
        st1.remove(21); // 21 -> la valeur est supprimé
        System.out.println(st1); 

        // SortedSet => tous les objets sont automatiquement triés lorsqu'ils sont ajoutés
        SortedSet<Double> st2 = new TreeSet<>();
        st2.add(5.6);
        st2.add(3.1);
        st2.add(10.7);
        System.out.println(st2);

        // - Interface Comparable
        // La classe (User) doit implémenter l'interface Comparable pour pouvoir faire
        // les comparaisons entre objet
        SortedSet<User> stUser = new TreeSet<>();
        stUser.add(new User("John", "Doe", 25));
        stUser.add(new User("Alan", "Smithee", 45));
        stUser.add(new User("Jane", "Doe", 28));
        System.out.println(stUser);

        // - Interface Comparator
        // Un Comparator est externe à la classe (user) dont on veut comparer les éléments
        // On peut créer plusieurs classes implémentant Comparator pour comparer de
        // manière différente les objets ( différent: attributs, ordre de trie)

        // L'objet Comparator est passé au constructeur de TreeSet 
        // * On peut utiliser une classe qui impléménte L'interface Comparator
        SortedSet<User> stUser2 = new TreeSet<>(new UserComparator());
        stUser2.add(new User("John", "Doe", 25));
        stUser2.add(new User("Alan", "Smithee", 45));
        stUser2.add(new User("Jane", "Doe", 28));
        System.out.println(stUser2);

        // * On peut aussi utiliser une Classe Annonyme pour définir le "Comparator"
        SortedSet<User> stUser3 = new TreeSet<>(new Comparator<User>() {

            @Override
            public int compare(User o1, User o2) {
                if (o1.getAge() < o2.getAge()) {
                    return -1;
                } else if (o1.getAge() == o2.getAge()) {
                    return 0;
                } else {
                    return 1;
                }
            }
        });
        stUser3.add(new User("John", "Doe", 25));
        stUser3.add(new User("Alan", "Smithee", 45));
        stUser3.add(new User("Jane", "Doe", 28));
        System.out.println(stUser3);

        // Queue -> fille d'attente
        Queue<Integer> pileFifo = new LinkedList<>();
        pileFifo.add(2);    
        pileFifo.offer(123);
        pileFifo.offer(23);
        pileFifo.offer(230);

        System.out.println(pileFifo.size());    // 4

        // peek => obtenir l'élément disponible, sans le retirer de la file d'attente
        System.out.println(pileFifo.peek()); // 2
        System.out.println(pileFifo.peek()); // 2
        System.out.println(pileFifo.size());    // 4
        
        // pool => obtenir l'élément disponible et le retirer de la file d'attente
        System.out.println(pileFifo.poll()); // 2
        System.out.println(pileFifo.size()); // 3

        // Map => association Clé/Valeur
        Map<String, User> m = new HashMap<>();
        m.put("st-12", new User("John", "Doe", 25));    // put => ajouter la clé "st-123" et la valeur associé (objet User)
        m.put("st-200", new User("Jane", "Doe", 25));
        m.put("st-23", new User("Jo", "Dalton", 25));
        m.put("st-2334", new User("Jo", "Aeffacer", 25));

        // get -> Obtenir la valeur associé à la clé st-12
        System.out.println(m.get("st-200"));
        
        System.out.println(m.size());// 4

        // containsKey => test si la clé fait partie de la map
        //                la comparaisson se fait avec equals
        System.out.println(m.containsKey("st-56")); // -> false, la clé fr-56 n'est pas présente dans la map
       
        // containsValue -> test si la valeur fait partie de la map
        System.out.println(m.containsValue(new User("John", "Doe", 25))); // -> true,la valeur est présente dans la map

        // Si la valeur existe déjà, elle est écrasée par la nouvelle valeur associé à la clé
        m.put("st-23", new User("Jack", "Dalton", 25));

        m.remove("st-2334");

        // keySet => retourne un Set qui contient toutes les clés de la map
        Set<String> keys = m.keySet();
        System.out.println(keys);

        // values => retourne une collection qui contient toutes les valeurs de la map
        Collection<User> values = m.values();
        System.out.println(values);

        // Entry => objet qui contient une clé et la valeur associée
        Set<Entry<String, User>> ent = m.entrySet();
        for (Entry<String, User> e : ent) {
            System.out.println("cle=" + e.getKey() + " valeur=" + e.getValue());
        }

        // isEmpty => test si la collection est vide
        System.out.println(m.isEmpty());
        m.clear();
        System.out.println(m.isEmpty());

        // Classe Collections => Classe utilitaires pour les collections
        Collections.sort(lstStr);   // sort -> trie
        System.out.println(lstStr);
        System.out.println(Collections.max(lstStr));    // max -> obetenir la valeur maximun d'uncollection
        Collections.shuffle(lstStr); // Mélange aléatoire des éléments de la collection
        System.out.println(lstStr);

        // Classe Arrays
        String[] tabStr = new String[5];
        Arrays.fill(tabStr, "Bonjour"); // => initialiser un tableau avec une valeur

        int t1[] = { 4, 1, -7, 10, 0 };
        int t2[] = { 4, 1, -7, 10, 0 };
        System.out.println(Arrays.equals(t1, t2));  // equals => comparaison de deux tableaux
        Arrays.sort(t1);                            // sort => tri d'un tableau
        System.out.println(Arrays.toString(t1));    // toString => afficher un tableau

        // binarySearch => recherche d'un élément dans un tableau trié
        System.out.println(Arrays.binarySearch(t1, 4)); // 3
        int index = Arrays.binarySearch(t1, 3); // <0
        System.out.println(index);
        System.out.println((index + 1) * -1); // s'il était présent dans le tableau il serait à index 3
   
        // Exercice Entreprise
        Entreprise en = new Entreprise("Acme");
        en.ajouter(new Employe("John", "Doe", TypeContrat.CDI, LocalDate.of(1997, 12, 9), 2500.0));
        en.ajouter(new Employe("Jane", "Doe", TypeContrat.CDI, LocalDate.of(1999, 1, 19), 2900.0));
        en.ajouter(new Employe("Alan", "Smithee", TypeContrat.CDD, LocalDate.of(1981, 6, 12), 3000.0));
        en.ajouter(new Employe("Jo", "Dalton", TypeContrat.STAGE, LocalDate.of(1989, 3, 14), 1800.0));
        System.out.println("nombre d'employé=" + en.getNombreEmploye());
        System.out.println("total salaire=" + en.totalSalaireMensuel());
        System.out.println("Salaire moyen=" + en.salaireMoyen());

    
    }

}
