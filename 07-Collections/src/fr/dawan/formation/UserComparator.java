package fr.dawan.formation;

import java.util.Comparator;

import fr.dawan.formation.beans.User;

public class UserComparator implements Comparator<User> {

    @Override
    public int compare(User o1, User o2) {
       if(o1.getAge()<o2.getAge()) {
           return 1;
       }else if(o1.getAge()==o2.getAge()){
           return 0; 
       } else {
           return -1;
       }
    }

}
