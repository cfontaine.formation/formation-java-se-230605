package fr.dawan.formation;

public class Main {

    public static void main(String[] args) {
      Terrain terrain=new Terrain();
      
      terrain.ajouter(new Rectangle(Couleur.ORANGE,1.0,1.0));
      terrain.ajouter(new Rectangle(Couleur.ORANGE,1.0,1.0));
      terrain.ajouter(new Cercle(Couleur.ROUGE,1.0));
      terrain.ajouter(new Cercle(Couleur.ROUGE,1.0));
      terrain.ajouter(new TriangleRectangle(Couleur.VERT,1.0,1.0));
      terrain.ajouter(new TriangleRectangle(Couleur.VERT,1.0,1.0));
      terrain.ajouter(new TriangleRectangle(Couleur.BLEU,1.0,1.0));
      terrain.ajouter(new Rectangle(Couleur.BLEU,1.0,1.0));
      System.out.println("Surface totale="+ terrain.calculSurface());
      System.out.println("Surface orange="+ terrain.calculSurface(Couleur.ORANGE));
      System.out.println("Surface rouge="+ terrain.calculSurface(Couleur.ROUGE));
      System.out.println("Surface vert="+ terrain.calculSurface(Couleur.VERT));
      System.out.println("Surface bleu="+ terrain.calculSurface(Couleur.BLEU));
    }

}
