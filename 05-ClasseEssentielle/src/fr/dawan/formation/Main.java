package fr.dawan.formation;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.Month;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.Date;
import java.util.StringTokenizer;

public class Main {

    public static void main(String[] args) {
        // => Chaine de caractères
        // Les chaines de caractères sont immuables une fois créée elles ne peuvent plus être modifiées
        String str1 = "Hello World";
        String str2 = new String("Bonjour");

        // length -> Nombre de caractère de la chaine de caractère
        System.out.println(str1.length());

        // Concaténation
        String strRes = str1 + str2; // concaténation +
        System.out.println(strRes);
        System.out.println(str1.concat(str2));

        // La méthode join concatène les chaines, en les séparants par une chaine de séparation
        String str3 = String.join(";", "azerty", "dfghj", "sbggl");
        System.out.println(str3);

        // Découpe la chaine et retourne un tableau de sous-chaine suivant un séparateur
        String[] tabStr = str3.split(";");

        for (String s : tabStr) {
            System.out.println(s);
        }

        // substring permet d'extraire une sous-chaine
        // de l'indice passé en paramètre jusqu'à la fin de la chaine
        System.out.println(str1.substring(6)); // World
        // de l'indice passé en paramètre jusqu'à l'indice de fin (exclut)
        System.out.println(str1.substring(6, 8)); // Wo

        // startsWith retourne true, si la chaine commence par la chaine passée en paramètre
        System.out.println(str1.startsWith("Hell")); // true
        System.out.println(str1.startsWith("aaa")); // false
        System.out.println(str1.endsWith("rld")); // true

        // indexOf retourne la première occurence de la chaine ou (de caractère) passée en paramètre
        System.out.println(str1.indexOf("o"));    // 4
        System.out.println(str1.indexOf("o", 5)); // 7
        System.out.println(str1.indexOf("o", 8)); // La sous-chaine "o" n'est pas trouvé -> -1
        
        // Retourne true, si la sous-chaine passée en paramètre est contenu dans la chaine
        System.out.println(str1.contains("o World")); // true
        System.out.println(str1.contains("azerty")); // false

        // Retourne le caractère à l'indice 1
        System.out.println(str1.charAt(4)); // o

        // Remplace toutes les les sous-chaines target par replacement
        System.out.println(str1.replace('o', 'O'));
        System.out.println(str1.replace("Wo", "__"));

        // Retourne la chaine en majuscule
        System.out.println(str1.toUpperCase());

        // trim supprime les caractères de blanc du début et de la fin de la chaine
        String str5 = "\t \n    hello world \n \n \t";
        System.out.println(str5);
        System.out.println(str5.trim());

        // %d -> entier, %f -> réel, %s -> string
        // Retourne une chaine formater
        System.out.println(String.format("[%d -> %s]", 42, str2));

        System.out.println(str2.equals("Bonjour"));
        System.out.println("Bonjour".equals(str2));

        String str6 = null;
        // System.out.println(str6.equals("Bonjour"));
        System.out.println("Bonjour".equals(str6));

        // Comparer les chaine 
        System.out.println(str1.compareTo(str2));       // >0 H>B => retourne une valeur 6, elle correspond à la distance en
        System.out.println(str2.compareTo(str1));       // <0 B<H => retourne une valeur -6
        System.out.println(str2.compareTo("Bonjour"));  // ==0 B==B  -> égal

        // On peut chainer les méthodes
        String strRes2 = str1.toLowerCase().substring(5).replace("o", "0");
        System.out.println(strRes2);

        // StringBuilder
        // Lorsqu'il y a beaucoup de manipulation (+de 3) sur une chaine ( concaténation
        // et insertion, remplacement,supression de sous-chaine)
        // il vaut mieux utiliser un objet StringBuilder qui est plus performant qu'un
        // objet String => pas de création d'objet intermédiare
        StringBuilder sb = new StringBuilder("Hello");
        sb.append(' ');
        sb.append("World");
        System.out.println(sb);
        sb.insert(5, "-----");
        System.out.println(sb);
        sb.delete(4, 10);
        System.out.println(sb);
        String str7 = sb.toString();
        System.out.println(str7);
        
        // StringTokenizer
        // Permet de décomposer une chaîne de caractères en une suite de mots séparés par des délimiteurs
        StringTokenizer st = new StringTokenizer("aze;rtu;uio;sdfghj;tt?yy;", ";?"); // ";,?" -> définie les délimiteurs ; , ?
        
        // countTokens => renvoie le nombre d’éléments
        System.out.println(st.countTokens()); // 6

        while (st.hasMoreTokens()) {            // hasMoreTokens => indique s'il reste des éléments à extraire
            System.out.println(st.nextToken()); // nextToken => renvoie l'élément suivant
        }

        // -----------------------------------------------------------------------------------
        // => Date
        // Date => JDK 1.0
        Date d = new Date();
        System.out.println(d);

        // à partir de java 8 => LocalDate, LocalTime, LocalDateTime
        // Les LocalDate, LocalTime, LocalDateTime sont immuables
        LocalDate dateDuJour = LocalDate.now(); // now => obtenir la date courante
        System.out.println(dateDuJour);

        // of => créer une date spécifique
        // LocalDate finAnnee=LocalDate.of(2023, 12, 31);
        LocalDate finAnnee = LocalDate.of(2023, Month.DECEMBER, 31);
        System.out.println(finAnnee);

        // On manipule une date avec les méthodes plusXXXXX et minusXXXXX
        LocalDate d1 = dateDuJour.plusMonths(2).minusYears(1).plusDays(3);
        System.out.println(d1);

        // Period => représente une durée
        Period deuxSemaine = Period.ofWeeks(2);
        System.out.println(dateDuJour.plus(deuxSemaine)); // plus et minus permettent d'ajouter ou retirer une Period à une LocalDate

        // Méthode format: LocalDate -> String 
        System.out.println(dateDuJour.format(DateTimeFormatter.ISO_DATE));
        System.out.println(dateDuJour.format(DateTimeFormatter.ofLocalizedDate(FormatStyle.SHORT)));
        System.out.println(dateDuJour.format(DateTimeFormatter.ofLocalizedDate(FormatStyle.FULL)));
        System.out.println(dateDuJour.format(DateTimeFormatter.ofPattern("dd/MM/YYYY")));

        // Méthode parse: String -> LocalDate
        LocalDate d3 = LocalDate.parse("2023-07-14");
        System.out.println(d3);
        
        LocalTime currentTime = LocalTime.now(); // heure courante
        System.out.println(currentTime);
    }

}
