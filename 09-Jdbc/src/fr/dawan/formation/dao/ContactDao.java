package fr.dawan.formation.dao;

import java.io.IOException;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import fr.dawan.formation.entities.Contact;

public class ContactDao extends AbstractDao<Contact> {

    @Override
    protected void create(Contact contact) throws SQLException, IOException {
        PreparedStatement ps = prepareStatementGeneratedKey("INSERT INTO contacts(prenom,nom,email,date_naissance) VALUES(?,?,?,?)");
        ps.setString(1, contact.getPrenom());
        ps.setString(2, contact.getNom());
        ps.setString(3, contact.getEmail());
        ps.setDate(4, Date.valueOf(contact.getDateNaissance()));
        ps.executeUpdate();
        ResultSet rs = ps.getGeneratedKeys();
        if (rs.next()) {
            contact.setId(rs.getLong(1));
        }
    }

    @Override
    protected void update(Contact element) throws SQLException, IOException {
        PreparedStatement ps = prepareStatement("UPDATE contacts SET prenom=?,nom=?,email=?,date_naissance=? WHERE id=?");
        ps.setString(1, element.getPrenom());
        ps.setString(2, element.getNom());
        ps.setString(3, element.getEmail());
        ps.setDate(4, Date.valueOf(element.getDateNaissance()));
        ps.setLong(5, element.getId());
        ps.executeUpdate();
    }

    @Override
    public void remove(long id) throws SQLException, IOException {
        PreparedStatement ps = prepareStatement("DELETE FROM contacts WHERE id=?");
        ps.setLong(1, id);
        ps.executeUpdate();
    }

    @Override
    public Contact findById(long id) throws SQLException, IOException {
        PreparedStatement ps = prepareStatement("SELECT id,prenom,nom,email,date_naissance FROM contacts WHERE id=?");
        ps.setLong(1, id);
        ResultSet rs = ps.executeQuery();
        if (rs.next()) {
            return createContactEntity(rs);
        }
        else {
            return null;
        }
    }

    @Override
    public List<Contact> findAll() throws SQLException, Exception {
        List<Contact> contacts = new ArrayList<>();
        PreparedStatement ps = prepareStatement("SELECT id,prenom,nom,email,date_naissance FROM contacts");
        ResultSet rs = ps.executeQuery();
        while (rs.next()) {
            contacts.add(createContactEntity(rs));
        }
        return contacts;
    }

    public List<Contact> findByEmail(String email) throws SQLException, IOException{
        List<Contact> contacts = new ArrayList<>();
        PreparedStatement ps = prepareStatement("SELECT id,prenom,nom,email,date_naissance FROM contacts WHERE email=?");
        ps.setString(1, email);
        ResultSet rs = ps.executeQuery();
        while (rs.next()) {
            contacts.add(createContactEntity(rs));
        }
        return contacts;
    }
    
    private Contact createContactEntity(ResultSet rs) throws SQLException {
        Contact c = new Contact(rs.getString("prenom"), rs.getString("nom"), rs.getString("email"),
                rs.getDate("date_naissance").toLocalDate());
        c.setId(rs.getLong("id"));
        return c;
        
    }

}
