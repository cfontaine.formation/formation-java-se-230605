package fr.dawan.formation.dao;

import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import java.util.Properties;

import fr.dawan.formation.entities.Entity;

public abstract class AbstractDao <T extends Entity>{
    // Chemin vers le fichier de propriété
    private static final String pathProperties = "db.properties";

    private static Connection cnx;

    private static Properties prop;

    // Obtenir la connexion à la base de donnée
    private static Connection getConnection() throws IOException, SQLException {
        if (cnx == null) { // cnx est null, la connexion à la bdd est fermée
            if (prop == null) {
                try (FileReader fr = new FileReader(pathProperties)) {
                    // Les paramètres de connexion de la base de données sont stockés
                    // dans un fichier de propriété
                    prop = new Properties();
                    prop.load(fr);
                }
            }
            // Créer la connexion à la base de donnée
            cnx = DriverManager.getConnection(prop.getProperty("url"), prop.getProperty("user"),
                    prop.getProperty("password"));
        }
        return cnx; // cnx est différent de null, la connexion existe

    }
    
    protected PreparedStatement prepareStatement(String requete) throws SQLException, IOException {
        return getConnection().prepareStatement(requete);
    }
    
    protected PreparedStatement prepareStatementGeneratedKey(String requete) throws SQLException, IOException {
        return getConnection().prepareStatement(requete,Statement.RETURN_GENERATED_KEYS);
    }

    public void close() {
        try {
            cnx.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        cnx = null;
    }
    
    public void saveOrUpdate(T entity) throws SQLException, IOException{
        if (entity.getId() == 0) {
            create(entity);
        } else {
            update(entity);
        }
    }
    
    public void remove(T entity) throws SQLException, IOException {
        remove(entity.getId());
    } 

    protected abstract void create(T element) throws SQLException, IOException;
    protected abstract void update(T element) throws SQLException, IOException;
    public abstract void remove(long id) throws SQLException, IOException;
    public abstract T findById(long id) throws SQLException, IOException;
    public abstract List<T> findAll() throws SQLException, Exception;
}
