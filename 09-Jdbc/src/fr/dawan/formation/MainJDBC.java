package fr.dawan.formation;

import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class MainJDBC {

    public static void main(String[] args) {
        // Test persistence d'un objet
        ContactJdbc c = new ContactJdbc("John", "Doe", "jd@dawan.com", LocalDate.of(2000, 01, 10));
        System.out.println(c);
        // insert(c);
        insertPrepared(c);
        System.out.println(c);

        List<ContactJdbc> lst = select();
        for (ContactJdbc contact : lst) {
            System.out.println(contact);
        }

        // Test d'une Transaction
        testTransaction(false); // OK
        testTransaction(true); // rollback
    }

    public static void insert(ContactJdbc c) {
        Connection cnx = null;
        try {
            // Chargement du pilote de la base de donnée (Avant java 6.0)
            Class.forName("org.h2.Driver");

            // Ouverture de la connexion à la base de donnée
            // utilisateur par défaut avec H2-> sa et pas de mot de passe
            // L'url avec h2
            // jdbc:h2:chemin_vers_la_base/Nom_base_de_donnée;AUTO_SERVER=TRUE;AUTO_SERVER_PORT=9090
            cnx = DriverManager.getConnection(
                    "jdbc:h2:C:/Dawan/Formations/bdd/contacts;AUTO_SERVER=TRUE;AUTO_SERVER_PORT=9090", "sa", "");

            // Statement => permet d'exécuter une requète SQL
            // s'il y a des paramètres, on va utiliser un PreparedStatement
            Statement st = cnx.createStatement();
            String reqsql = "INSERT INTO contacts(prenom,nom,email,date_naissance) VALUES ('" + c.getPrenom() + "','"
                    + c.getNom() + "','" + c.getEmail() + "','" + c.getDateNaissance() + "')";

            // Il faut utiliser la méthode executeUpdate pour les requêtes INSERT, DELETE, UPDATE
            // Statement.RETURN_GENERATED_KEYS indique que l'on veut récupérer la clé primaire générée par la bdd
            st.executeUpdate(reqsql, Statement.RETURN_GENERATED_KEYS);

            // Récupérer la valeur de la clé primaire générée par la base de donnée
            ResultSet rs = st.getGeneratedKeys();
            if (rs.next()) {
                c.setId(rs.getLong(1));
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (cnx != null) {
                try {
                    // Fermeture de la connexion à la base de donnée
                    cnx.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }

    }

    public static void insertPrepared(ContactJdbc c) {
        Connection cnx = null;
        try {
            Class.forName("org.h2.Driver");
            cnx = DriverManager.getConnection(
                    "jdbc:h2:C:/Dawan/Formations/bdd/contacts;AUTO_SERVER=TRUE;AUTO_SERVER_PORT=9090", "sa", "");

            // PreparedStatement => permet d'exécuter aussi une requête SQL
            // à privilégier, si la requête contient des paramètres: permet d'éviter les
            // Statement.RETURN_GENERATED_KEYS indique que l'on veut récupérer la clé primaire générée par la bdd
            PreparedStatement ps = cnx.prepareStatement(
                    "INSERT INTO contacts(prenom,nom,email,date_naissance) VALUES (?,?,?,?)",
                    Statement.RETURN_GENERATED_KEYS);

            // Les méthodes setString, setDouble ... permettent de donnée des valeurs aux paramètres de la requête
            // L'indice commence à 1
            ps.setString(1, c.getPrenom());
            ps.setString(2, c.getNom());
            ps.setString(3, c.getEmail());
            ps.setDate(4, Date.valueOf(c.getDateNaissance())); // Date.valueOf pour convertir un LocalDate en
                                                               // java.sql.Date

            // Il faut utiliser la méthode executeUpdate pour les requêtes INSERT, DELETE, UPDATE
            ps.executeUpdate();

            // Récupérer la valeur de la clé primaire générée par la base de donnée
            ResultSet rs = ps.getGeneratedKeys();
            if (rs.next()) {
                c.setId(rs.getLong(1));
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (cnx != null) {
                try {
                    // Fermeture de la connexion à la base de donnée
                    cnx.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }

    }

    public static List<ContactJdbc> select() {
        // à partir de JDBC 4, il n'est plus necessaire de charger le driver avec Class.forName
        List<ContactJdbc> contacts = new ArrayList<>();
        
        // On peut utiliser un try with ressource pour fermer automatiquement la connexion à la base de donnée
        try (Connection cnx = DriverManager.getConnection("jdbc:h2:C:/Dawan/Formations/bdd/contacts;AUTO_SERVER=TRUE;AUTO_SERVER_PORT=9090", "sa", "")) {
            Statement stm = cnx.createStatement();
            
            // Il faut utiliser la méthode executeQuery pour les requêtes SELECT
            // ResultSet => pour récupèrer les résultats de la requête
            ResultSet rs = stm.executeQuery("SELECT id,prenom,nom,email,date_naissance FROM contacts");
            
            // La méthode next -> passe au résultat suivant (ligne), retourne vrai tant qu'il y a des résultats
            // (au départ, on se trouve juste avant le premier résultat (première ligne))
            while (rs.next()) {
                // Les Méthodes getString, getDouble, ... permettent de lire les valeurs des colonnes
                ContactJdbc c = new ContactJdbc(rs.getString("prenom"), rs.getString("nom"), rs.getString("email"),
                        rs.getDate("date_naissance").toLocalDate());    // toLocalDate pour convertir java.sql.Date en LocalDate
                c.setId(rs.getLong("id"));
                contacts.add(c);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return contacts;
    }
    
    // Exercice: écrire une méthode pour supprimer un contact
    public static void delete(ContactJdbc c) {
        try (Connection cnx = DriverManager.getConnection("jdbc:h2:C:/Dawan/Formations/bdd/contacts;AUTO_SERVER=TRUE;AUTO_SERVER_PORT=9090", "sa", "")) {
            PreparedStatement ps = cnx.prepareStatement("DELETE FROM contacts WHERE id=?");
            ps.setLong(1, c.getId());
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    // Exemple test des transactions
    // Si error est à true, on génére une SqlException entre les 2 requêtes
    // Aucune des 2 n'est executé => rollback, annule la première requête
    public static void testTransaction(boolean error) {
        Connection cnx = null;
        try {
            cnx = DriverManager.getConnection("jdbc:h2:C:/Dawan/Formations/bdd/contacts;AUTO_SERVER=TRUE;AUTO_SERVER_PORT=9090", "sa", "");
            cnx.setAutoCommit(false); // Désactivation du mode auto-commit
            PreparedStatement ps = cnx.prepareStatement("INSERT INTO contacts(prenom,nom,email,date_naissance) "
                    + "VALUES('Jane','Doe','ja@dawan.com','1998-03-06')");
            ps.executeUpdate();
            if (error) {
                throw new SQLException();
            }
            ps = cnx.prepareStatement("INSERT INTO contacts(prenom,nom,email,date_naissance) "
                    + "VALUES('Alan','Smithee','al@dawan.com','1956-03-06')");
            ps.executeUpdate();
            cnx.commit(); // commit() => permet de valider les requêtes
        } catch (SQLException e) {
            try {
                cnx.rollback(); // rollback() => pour annuler les requêtes
            } catch (SQLException e1) {
                e.printStackTrace();
            }
            System.out.println("rollback");
        } finally {
            if (cnx != null) {
                try {
                    cnx.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }

    }
}

