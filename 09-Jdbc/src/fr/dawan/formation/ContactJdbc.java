package fr.dawan.formation;

import java.io.Serializable;
import java.time.LocalDate;

public class ContactJdbc implements Serializable {

    private static final long serialVersionUID = 1L;

    private long id;

    private String prenom;

    private String nom;

    private String email;

    private LocalDate dateNaissance;

    public ContactJdbc() {
    }

    public ContactJdbc(String prenom, String nom, String email, LocalDate dateNaissance) {
        this.prenom = prenom;
        this.nom = nom;
        this.email = email;
        this.dateNaissance = dateNaissance;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public LocalDate getDateNaissance() {
        return dateNaissance;
    }

    public void setDateNaissance(LocalDate dateNaissance) {
        this.dateNaissance = dateNaissance;
    }

    @Override
    public String toString() {
        return "Contact [id=" + id + ", prenom=" + prenom + ", nom=" + nom + ", email=" + email + ", dateNaissance="
                + dateNaissance + "]";
    }

}
