package fr.dawan.formation;

import java.time.LocalDate;
import java.util.List;

import fr.dawan.formation.dao.ContactDao;
import fr.dawan.formation.entities.Contact;

public class MainDAO {

    public static void main(String[] args) {
        Contact c = new Contact("Jo", "Dalton", "jd@daan.com", LocalDate.of(1987, 4, 5));
        ContactDao dao = new ContactDao();
        try {
            dao.saveOrUpdate(c);
            System.out.println(dao.findById(c.getId()));
             c.setEmail("aa@dawan.fr");
            dao.saveOrUpdate(c);
            List<Contact> contacts = dao.findAll();
            for (Contact co : contacts) {
                System.out.println(co);
            }
            dao.remove(c);
            contacts = dao.findAll();
            for (Contact co : contacts) {
                System.out.println(co);
            }
            dao.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
