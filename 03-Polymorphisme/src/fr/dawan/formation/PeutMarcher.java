package fr.dawan.formation;

public interface PeutMarcher {
    
    int VITESSE_MAX=30; // en java 8
    
    static int getNbPatte() {
        return 4;
    }
    
   /*public  abstract*/  void marcher();
   
   void courrir();
   
  default void ramper() { // en java 8
      System.out.println("méthode par défaut ramper");
  }

}
