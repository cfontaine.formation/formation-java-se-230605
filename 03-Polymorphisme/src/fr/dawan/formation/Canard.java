package fr.dawan.formation;

public class Canard extends Animal implements PeutMarcher,PeutVoler {

    public Canard(int poid, int age) {
        super(poid, age);
        // TODO Auto-generated constructor stub
    }

    @Override
    public void emettreSon() {
        // TODO Auto-generated method stub
        
    }

    @Override
    public void decoller() {
       System.out.println("Décoller");
        
    }

    @Override
    public void atterir() {
        System.out.println("Attérir");
        
    }

    @Override
    public void marcher() {
        System.out.println("Le canard marche");
        
    }

    @Override
    public void courrir() {
        System.out.println("Le canard court");
        
        
    }

    @Override
    public void ramper() {
        System.out.println("Le canard rampe");
        
    }

    
}
