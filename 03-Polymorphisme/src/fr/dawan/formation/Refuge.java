package fr.dawan.formation;

public class Refuge {
    
    private Animal[] places=new Animal[10];
    
    private int nbPlaceOccupe;

    public void ajouter(Animal a) {
        if(nbPlaceOccupe<10) {
            places[nbPlaceOccupe]=a;
            nbPlaceOccupe++;
        }
    }
    
    public void ecouter() {
        for(int i=0;i<nbPlaceOccupe;i++) {
            places[i].emettreSon();
        }
    }
    
}
