package fr.dawan.formation;

public enum Motorisation {
    ESSENCE,  DIESEL, ELECTRIQUE, HYDROGENE, GPL
}
