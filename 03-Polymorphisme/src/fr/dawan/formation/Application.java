package fr.dawan.formation;

public class Application {

    public static void main(String[] args) {
//        Animal a1 = new Animal(3000, 3);
//        a1.emettreSon();
//        System.out.println(a1.getAge());

        Chien ch1 = new Chien(7000, 4, "Rolo");
        ch1.emettreSon();
        System.out.println(ch1.getNom());
        
        // upcasting (enfant -> parent): implicite
        Animal a2 = new Chien(4000, 10, "Laika");
        a2.emettreSon();
        
        if (a2 instanceof Chien) {  // instanceof => permet de tester si a2 est bien de "type" Chien
            // Downcasting (parent -> enfant): necessite un cast
            Chien ch2 = (Chien) a2;
            ch2.emettreSon();
            System.out.println(ch2.getNom());
        }
        
        Refuge r=new Refuge();
        r.ajouter(new Chien(7000, 4, "Rolo"));
        r.ajouter(new Chat(5000,14));
        r.ajouter(new Chien(4000, 10,  "Laika"));
        r.ajouter(new Chat(3000,4));
        r.ecouter();
        
        // Object
        // ToString
        Animal a3=new Chien(3000,5,"Idefix");
        System.out.println(a3.toString());
        System.out.println(a3);
        
        // Equals
        Chien ch3=new Chien(4000,6,"Tom");
        Chien ch4=new Chien(4000,6,"Tom");
        System.out.println(ch3==ch4); // false
        System.out.println(ch3.equals(ch4)); 
        
        // Clone
        try {
            Chien ch5=(Chien)ch3.clone();
            System.out.println(ch5);
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        
        // Interface
        PeutMarcher p1=new Chien(3000,2, "tom");
        p1.courrir();
        p1.marcher();
        
        PeutMarcher p2=new Canard(3000,1);
        p2.marcher();
        
        // Enumération
        Motorisation mt=Motorisation.ESSENCE;
        System.out.println(mt); // mt.toString()
        System.out.println(mt.name());
        
        System.out.println(mt.ordinal());
        
        Motorisation[] tabMt=Motorisation.values();
        for(Motorisation e: tabMt) {
            System.out.println(e);
        }
        
        String str="GPL";
        Motorisation mt2=Motorisation.valueOf(str);
        System.out.println(mt2);
        
        // interface en java 8
        System.out.println(PeutMarcher.VITESSE_MAX);
        System.out.println(Chien.VITESSE_MAX);
        
        System.out.println(PeutMarcher.getNbPatte());
        //System.out.println(Chien.getNbPatte());
        
        // interface par défaut
        p1.ramper();
        p2.ramper();
        
    }
    
 

}
