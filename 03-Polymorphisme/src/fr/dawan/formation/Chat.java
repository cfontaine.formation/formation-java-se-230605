package fr.dawan.formation;

public class Chat extends Animal {

    public Chat(int poid, int age) {
        super(poid, age);
    }

    @Override
    public void emettreSon() {
        System.out.println("Le chat miaule");
    }

}
