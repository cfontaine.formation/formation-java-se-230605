import java.util.Scanner;

public class Tableau {

    public static void main(String[] args) {
        // Déclaration d'un tableau à une dimension

        double[] tt = null; // On déclare une référence vers le tableau
        tt = new double[10]; // On crée l'objet tableau de double

        double[] t = new double[10]; // ou double t[]=new double[10];
        // Valeur d'initialisation des éléments du tableau
        // entier -> 0
        // double ou float -> 0.0
        // char -> '\u0000'
        // boolean -> false
        // référence -> null;

        // Accèder à un élément du tableau
        System.out.println(t[0]); // -> 0 accès au 1er élément
        t[1] = 1.23; // accès au 2ème élément
        System.out.println(t[1]);

        // Indice en dehors du tableau => génére une exception
        // t[40]=3.4; // -> génére une exception

        // Nombre d'élément du tableau
        System.out.println(t.length); // 10

        // Parcourir un tableau avec un for
        for (int i = 0; i < t.length; i++) {
            System.out.println("t[" + i + "]=" + t[i]);
        }

        // Parcourir un tableau avec un "foreach" (uniquement en lecture avec les types primitifs)
        for (double elm : t) {
            System.out.println(elm);
        }

        // Déclaration et initialisation du tableau
        int[] ti = { 3, 67, 23, 68 };
        for (int e : ti) {
            System.out.println(e);
        }

        // Quand on crée un tableau, le nombre d'élémént peut être une variable de type entière
        int s = 6;
        boolean[] tb = new boolean[s];
        System.out.println(tb.length); // tableau de 6 éléments

        // Tableau 1D
        // 1 Trouver la valeur maximale et la moyenne d’un tableau de 5 entiers:
        // -7,-4,-8,-10,-3
        // 2 Modifier le programme pour faire la saisie de la taille du tableau et
        // saisir les éléments du tableau
        // int tab[]= {-7,-4,-8,-10,-3}; // 1

        Scanner scan = new Scanner(System.in); // 2
        int size = scan.nextInt();
        int tab[] = new int[size];
        for (int i = 0; i < tab.length; i++) {
            tab[i] = scan.nextInt();
        }

        int max = tab[0]; // Integer.MIN_VALUE
        int somme = 0;
        for (int elm : tab) {
            somme += elm;
            if (elm > max) {
                max = elm;
            }
        }
        double moyenne = ((double) somme) / tab.length;
        System.out.println("maximum=" + max + " moyenne=" + moyenne);

        // Obtenir le maximum d'un tableau
        // Arrays.stream(tab).max().getAsInt();

        // Tableau à 2 dimensions
        int[][] tab2d = new int[3][4];

        // Accèder à un élément
        System.out.println(tab2d[0][0]);
        tab2d[1][2] = 42;

        // Nombre de ligne
        System.out.println(tab2d.length); // 3

        // Nombre de colonne de la première ligne
        System.out.println(tab2d[0].length); // 4

        // Parcourir un tableau à 2 dimensions avec un for
        // autant de boucle imbriquée qu'il y a de dimension
        for (int i = 0; i < tab2d.length; i++) {
            for (int j = 0; j < tab2d[i].length; j++) {
                System.out.print(tab2d[i][j] + "\t");
            }
            System.out.println();
        }

        // Déclaration et initialisation d'un tableau en 2D
        String[][] tabStr = { { "az", "er", "ty" }, { "sd", "fg", "rg" } };
        for (int i = 0; i < tabStr.length; i++) {
            for (int j = 0; j < tabStr[i].length; j++) {
                System.out.print(tabStr[i][j] + "\t");
            }
            System.out.println();
        }

        // Tableau à 3 dimensions
        double[][][] tab3d = new double[3][4][2];

        // Tableau en escalier
        // Déclaration d'un tableau en escalier => chaque ligne a un nombre d'élement différent
        double[][] tEsc = new double[3][];
        tEsc[0] = new double[2];
        tEsc[1] = new double[4];
        tEsc[2] = new double[3];

        // Parcourir un Tableau en escalier avec un for
        for (int i = 0; i < tEsc.length; i++) {
            for (int j = 0; j < tEsc[i].length; j++) {
                System.out.print(tEsc[i][j] + "\t");
            }
            System.out.println();
        }

        // Parcourir un Tableau en escalier avec un foreach
        for (double[] row : tEsc) {
            for (double elm : row) {
                System.out.print(elm + "\t");
            }
            System.out.println();
        }
        
        scan.close();
    }
}
