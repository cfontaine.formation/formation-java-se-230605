import java.util.Scanner;

public class Instruction {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        double d = sc.nextDouble();

        // Condition if
        if (d > 10.0) {
            System.out.println("d supérieur à 10");
        } else if (d < 10.0) {
            System.out.println("d inférieur à 10");
        } else {
            System.out.println("d égal à 10");
        }

        // Exercice: Nombre Pair
        // Créer un programme qui indique, si le nombre entier
        // saisie dans la console est paire ou impaire
        int val = sc.nextInt();
        if (val % 2 == 0) { // ou aussi pour tester la parité (val & 1) == 0
            System.out.println("Paire");
        } else {
            System.out.println("Impaire");
        }

        // Exercice: Interval
        // Saisir un nombre et dire s'il fait parti de l'intervalle -4 (exclus) et 7 (inclus)
        System.out.println("Saisir un entier:");
        int v = sc.nextInt();
        if (v > -4 && v <= 7) {
            System.out.println(v + " fait parti de l'interval");
        }

        // Condition switch
        System.out.println("Entre un entier entre 1 et 7:");
        int jours = sc.nextInt();
        switch (jours) {
        case 1:
            System.out.println("Lundi");
            // break; // Comme il n'y a pas de break le code continura à s'executer jusqu'a
                      // la fin du switch ou jusqu'au prochain break
        case 6:
        case 7:
            System.out.println("week end !");
            break;
        default:
            System.out.println("autre jour");
        }

        // Exercice: Calculatrice
        // Faire un programme calculatrice
        // Saisir dans la console
        // - un double v1
        // - une chaine de caractère opérateur qui a pour valeur valide : + - * /
        // - un double v2
        // Afficher:
        // - Le résultat de l’opération
        // - Une message d’erreur si l’opérateur est incorrecte
        // - Une message d’erreur si l’on fait une division par 0
        System.out.println("Calculatrice");
        double v1 = sc.nextDouble();
        String op = sc.next();
        double v2 = sc.nextDouble();
        switch (op) {
        case "+":
            System.out.println(v1 + " + " + v2 + " =" + (v1 + v2));
            break;
        case "-":
            System.out.println(v1 + " - " + v2 + " =" + (v1 - v2));
            break;
        case "x":
            System.out.println(v1 + " x " + v2 + " =" + (v1 * v2));
            break;
        case "/":
            if (v2 == 0.0) { // (v2>-0.000001 && v2<0.000001)
                System.err.println("Division par 0");
            } else {
                System.out.println(v1 + " / " + v2 + " =" + (v1 / v2));
            }
            break;
        default:
            System.err.println("L'opérateur " + op + " n'existe pas");
        }

        // Opérateur ternaire
        System.out.println("Saisir un double");
        double v3 = sc.nextDouble();
        double vAbs = v3 > 0 ? v3 : -v3;
        System.out.println("|" + v3 + "|= " + vAbs);

        // boucle while
        int j = 0;
        while (j < 10) {
            System.out.println(j);
            j++;
        }

        // boucle do while
        j = 0;
        do {
            System.out.println(j);
            j++;
        } while (j < 10);

        // boucle for
        for (int i = 0; i < 10; i += 2) {
            System.out.println(i);
        }

        // Instructions de branchement
        // break
        j = 0;
        for (;;) {
            j++;
            if (j == 3) {
                break; // break => termine la boucle
            }
            System.out.println(j);
        }

        // continue
        for (int i = 0; i < 10; i++) {
            if (i == 3) {
                continue; // continue => on passe à l'itération suivante
            }
            System.out.println(i);
        }

        // Label
        // break avec Label:
        EXIT_LOOP: for (int i = 0; i < 10; i++) {
            for (int k = 0; k < 5; k++) {
                if (i == 1) {
                    break EXIT_LOOP;    // sortie de 2 boucles imbriquées
                }
                System.out.println(i + " " + k);
            }
        }

//      Table de multiplication
//      Faire un programme qui affiche la table de multiplication pour un nombre entre 1 et 9  
//
//          1 X 4 = 4
//          2 X 4 = 8
//          …
//          9 x 4 = 36
//          
//      Si le nombre saisie est en dehors de l’intervalle 1 à 9 on arrête sinon on redemande une nouvelle valeur
        System.out.println("Saisir un nombre entre 1 et 9");
        for (;;) {  // ou while(true) => boucle infinie
            int m = sc.nextInt();
            if (m < 1 || m > 9) {
                break;
            }
            for (int i = 1; i < 10; i++) {
                System.out.println(i + " x " + m + " = " + (m * i));
            }
        }
        
        // ou
//      int mul = 1;
//      while (mul >= 1 && mul <= 9) {
//          mul = sc.nextInt();
//          if (mul >= 1 && mul <= 9) {
//              for (int i = 1; i < 10; i++) {
//                  System.out.println(i + " x " + mul + " = " + (mul * i));
//              }
//          }
//      }

        sc.close();
    }

}
