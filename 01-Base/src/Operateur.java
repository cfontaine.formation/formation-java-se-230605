import java.util.Scanner;

public class Operateur {

    public static void main(String[] args) {
        // Opérateur arithmétique
        int a = 1;
        int b = 3;
        int res = a + b;
        System.out.println(res);
        System.out.println(b % 2); // 1 % reste de la division entière

        // Division par 0:
        // entier -> exception
        // System.out.println(10/0); // => exception

        // Nombre à virgule flottante
        System.out.println(0.0 / 0.0); // NAN
        System.out.println(1.0 / 0.0); // INFINITY
        System.out.println(-1.0 / 0.0); // -INFINITY

        // Incrémenration/ Décrémentation
        // Pré-incrémention
        int inc = 0;
        int r = ++inc; // Incrémentation de inc et affectation de r avec la valeur de inc
        System.out.println(inc + " " + r); // inc= 1 r=1

        // Post-incrémentaion
        inc = 0;
        r = inc++; // Affectation de r avec la valeur de inc et incréméntation de inc
        System.out.println(inc + " " + r); // r=0 inc=1

        // Affectation composé
        int af = 12;
        r += af; // équivalant à r=r+af

        // Opérateur comparaison
        // Une comparaison a pour résultat un booléen
        boolean tst = af > 100;
        System.out.println(tst);// false

        // Opérateur logique
        // ! => Opérateur non
        System.out.println(!tst); // true

        // c1   c2 | ET  OU  Ou exclusif
        //---------+------------------
        // F    F  | F   F      F
        // F    T  | F   T      T
        // T    F  | F   T      T
        // T    T  | T   T      F

        // Opérateur court circuit && et ||
        // && -> Opérateur et
        tst = af > 100 && af < 20;
        // && dés qu'une comparaison est fausse, les suivantes ne sont pas évaluée
        System.out.println(tst + " " + af);

        // || -> Opérateur ou
        // || dés qu'une comparaison est vrai, les suivantes ne sont pas évaluées
        tst = af < 100 || af == 20;
        System.out.println(tst + " " + af);

        // Opérateur binaire
        // Littéraux entier changement de base
        int dec = 12;       // par défaut -> base 10
        int hexa = 0xF23E;  // 0x -> base 16 Héxadécimal
        int octal = 045;    // 0  -> base 8  Octal
        int bin = 0b101101; // 0b -> base 2  Binaire
        System.out.println(dec + " " + hexa + " " + octal + " " + bin);

        // Opérateur binaire
        bin = 0b10101;
        System.out.println(Integer.toBinaryString(~bin));          // 11111111 11111111 11111111 11101010 -> Complément
        System.out.println(Integer.toBinaryString(bin & 0b11001)); // 10001 -> Et
        System.out.println(Integer.toBinaryString(bin | 0b11001)); // 11101 -> Ou
        System.out.println(Integer.toBinaryString(bin ^ 0b11001)); // 1100  -> Ou exclusif

        // Opérateur Decallage
        // Décallage à droite de 2 bits, on insére 2 fois le bit de signe à gauche
        System.out.println(Integer.toBinaryString(bin >> 2)); // 101
        // Décallage à droite de 2 bits, on insére 2 fois 0
        System.out.println(Integer.toBinaryString(bin >> 2)); // 101
        // Décallage à gauche de 1 bit, on insère un 0 à droite
        System.out.println(Integer.toBinaryString(bin << 1)); // 101010

        // Promotion numérique
        // 1=> Le type le + petit est promu vers le + grand type des deux
        int pr1 = 12;
        long pr2 = 34L;
        long pres = pr1 + pr2;  // 4=> Après une promotion le résultat aura le même type
        System.out.println(pres);

        // 2=> La valeur entière est promue en virgule flottant
        double pr3 = 3.12;
        double pres2 = pr1 + pr3;
        System.out.println(pres2);
        
        boolean tst2 = pres2 == 15.12;
        System.out.println(tst2);

        int pr4 = 11;
        int pres3 = pr4 / 2;
        System.out.println(pres3); // 5

        double pres4 = pr4 / 2.0; // 5.5
        System.out.println(pres4);
        double pres5 = ((double) pr4) / 2; // 5.5
        System.out.println(pres5);

        // 3=> byte, short, char sont promus en int
        short s1 = 1;
        short s2 = 3;
        int s3 = s1 + s2;
        System.out.println(s3);

        // Saisie dans la console => Scanner
        Scanner sc = new Scanner(System.in);
        int v=sc.nextInt();     
        String str=sc.next();
        double d=sc.nextDouble();
        System.out.println(v + " " + str + " " +d);

        // Exercice: Moyenne
        // Saisir 2 nombre entier et afficher la moyenne des 2 nombres
        System.out.println("Saisir 2 entiers:");
        int m1=sc.nextInt();
        int m2=sc.nextInt();
        double moyenne = (m1+m2)/2.0;
        System.out.println("moyenne=" + moyenne);

        sc.close(); // une fois fermé, on ne peut plus créer un nouveau scanner -> le flux d'entrée est fermé
    }

}
