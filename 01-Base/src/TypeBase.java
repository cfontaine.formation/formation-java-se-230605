
public class TypeBase {

    // Méthode main => point d'entré du programme
    public static void main(String[] args) {
        // Déclaration variable
        int i;

        // System.out.println(i); // En java, on ne peut pas utiliser une variable non
        // initialisée

        // Initialisation de la variable
        i = 42;
        System.out.println(i);

        // Déclaration et initialisation de la variable
        double d = 1.23;
        System.out.println(d);

        // Déclaration multiple
        double hauteur = 12.3, largeur = 45.6;
        System.out.println(hauteur + " " + largeur);

        // Littérale
        // Littérale entière est par défaut de type int
        long l = 123456779012L; // L -> long
        System.out.println(l);

        // Littéral réel
        double d1 = 1.3;
        double d2 = .5;
        double d3 = 2.5e3; // 2500
        System.out.println(d1 + " " + d2 + " " + d3);

        // Littérale réel par défaut double
        float f = 1.2F; // F -> float
        System.out.println(f);

        // Littérale boolean
        boolean b = true; // ou false
        System.out.println(b);

        // Littérale caractère
        char chr = 'a';
        char chrUtf8 = '\u0061';
        System.out.println(chr + " " + chrUtf8);

        int j = i; // j=42
        System.out.println(i + " " + j);
               
        // Transtypage implicite => pas de perte de donnée
        // Type inférieur vers un type supérieur
        int ti1 = 1;
        long ti2 = ti1;
        // Entier vers un réel
        double ti3 = ti1;
        System.out.println(ti1 + " " + ti2 + " " + ti3);

        // Transtype explicite cast => (nouveauType)
        // Type supérieur vers un type inférieur
        int te1 = 34;
        short se1 = (short) te1;
        System.out.println(te1 + " " + se1);
        // Réel vers un entier
        double td2 = 34.5;
        int te2 = (int) td2;
        System.out.println(td2 + " " + te2);

        // Transtype explicite: Dépassement de capacité
        int dep1 = 300;          // 00000000 00000000 0000001 00101100 -> 300
        byte dep2 = (byte) dep1; // 00101100 -> 44
        System.out.println(dep1 + " " + dep2);

        // Type Référence
        StringBuilder sb1 = new StringBuilder("hello");
        StringBuilder sb2 = null;   // sb2 ne référence aucun objet
        System.out.println(sb1 + " " + sb2);

        sb2 = sb1;  // sb1 et sb2 font références au même objet
        System.out.println(sb1 + " " + sb2);

        sb1 = null;
        System.out.println(sb1 + " " + sb2);

        sb2 = null;
        System.out.println(sb1 + " " + sb2);
        // sb1 et sb2 sont égales à null
        // Il n'y a plus de référence sur l'objet 
        // => Il est éligible à la destruction par le garbage collector
    }

}
