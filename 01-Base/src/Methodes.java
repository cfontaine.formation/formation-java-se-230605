import java.util.Scanner;

public class Methodes {

    public static void main(String[] args) {
        // Appel d'une méthode
        double m = multiplication(2.0, 3.0);
        System.out.println(m);
        
        // Appel d'une méthode sans type retour (void)
        afficher(3);

        // Test des arguments par valeurs
        int v = 1;
        testParamValeur(v); // La valeur contenu dans v est copier dans le paramètre (a)
        System.out.println(v); // le contenu de v ne peut pas être modifié par la méthode => passage par valeur

        // Test des arguments par valeurs (paramètre de type référence)
        StringBuilder b = new StringBuilder("Value");
        testParamValueRef(b);
        System.out.println(b);

        // Test des arguments par valeurs (Modification de l'état de l'objet)
        testParamValueRefObj(b);
        System.out.println(b);

        // Exercice: appel de la méthode maximum
        Scanner sc = new Scanner(System.in);
        System.out.println("Saisir 2 entiers");
        int i1 = sc.nextInt();
        int i2 = sc.nextInt();
        int max = maximum(i1, i2);
        System.out.println("Le maximum=" + max);

        // Exercice: appel de la méthode paire
        int n = sc.nextInt();
        System.out.println(even(n));

        // Nombre d'arguments variable
        double moy = moyenne(2.0, 7.5, 4.5);
        System.out.println(moy);
        System.out.println(moyenne(15.5));
        System.out.println(moyenne());

        double[] n2 = { 3.5, 7.4, 6.7 };
        System.out.println(moyenne(n2));

        // Surcharge de méthode
        // Correspondance exacte
        System.out.println(somme(1, 3));
        System.out.println(somme(1, 3.0));
        System.out.println(somme(1.5, 3.0));
        System.out.println(somme("az", "erty"));
        System.out.println(somme(1, 3, 5));
        
        // Si le compilateur ne trouve pas de corresponcance exacte avec le type des paramètres
        // il effectue des conversions automatique pour trouver la méthode à appeler
        System.out.println(somme(1, 3L));       // appel de la méthode => somme(int,double)
        System.out.println(somme('a', 'z'));    // appel de la méthode => somme(int, int )

        Integer iw = Integer.valueOf(34);
        System.out.println(somme(iw, 5));   // appel de la méthode => somme(int, int )

        System.out.println(somme(3, 4, 6, 7, 8));

        // Si le compilateur ne trouve pas de méthode correspondante aux paramètres => ne compile pas
        // System.out.println(somme(1,"AZERTY"));

        // Méthode Récursive
        int res = factorial(3);
        System.out.println(res);

        // Affichage des paramètres passés à la méthode main
        for (String s : args) {
            System.out.println(s);
        }
        
        // Exercice: tableau
        menu(sc);
        sc.close();
    }

    // Déclaration d'une méthode
    static double multiplication(double d1, double d2) {
        return d1 * d2; // return => Interrompt l'exécution de la méthode
                        // => Retourne la valeur (expression à droite)
    }

    static void afficher(int a) {
        System.out.println(a);
        // return; // avec void => return; ou return peut être omis
    }

    // Test des arguments par valeurs (paramètre de type primitif)
    static void testParamValeur(int a) {
        System.out.println(a);
        a = 23; // la modification du paramètre a n'a pas de répercution en dehors de la méthode
        System.out.println(a);
    }
    
    // test des arguments par valeurs (paramètre de type référence)
    public static void testParamValueRef(StringBuilder sb) {
        System.out.println(sb);
        // si on modifie la valeur de la référence sb, il n'a pas répercution en dehors
        // de la méthode
        // la référence est modifiée
        sb = new StringBuilder("Other Value");
        System.out.println(sb);
    }

    // test des arguments par valeurs (Modification de l'état de l'objet)
    public static void testParamValueRefObj(StringBuilder sb) {
        System.out.println(sb);
        // si on modifie l'état de l'objet, il a une répercution en dehors de la méthode
        // (la référence n'est pas modifiée)
        sb.append("et Other Value");
        System.out.println(sb);
    }



    // Exercice Maximum
    // Écrire une fonction maximum qui prends en paramètre 2 nombres et retourne le
    // maximum
    // Saisir 2 nombres et afficher le maximum entre ces 2 nombres
    static int maximum(int a, int b) {
        return a > b ? a : b;
        //ou
//        if(a>b) {
//            return a;
//        }
//        else {
//            return b;
//        }
    }
    
    // Exercice Paire
    // Écrire une fonction Even qui prend un entier en paramètre
    // elle retourne vrai si il est paire
    static boolean even(int n) {
        return n % 2 == 0;
        // ou
//        if(n%2==0) {
//            return true;
//        }
//        else {
//            return false;
//        }
    }

    // Nombre d'arguments variable
    static double moyenne(double... notes) {
        // dans la méthode notes est considérée comme un tableau
        double somme = 0.0;
        for (double n : notes) {
            somme += n;
        }
        return somme / notes.length;
    }

    // Surcharge de méthode
    // Une méthode peut être surchargée => plusieurs méthodes peuvent avoir le même
    // nom, mais leurs signatures doient être différentes
    // La signature d'une méthode correspond aux types et nombre de paramètres
    // Le type de retour ne fait pas partie de la signature
    static int somme(int a, int b) {
        System.out.println("2 entiers");
        return a + b;
    }

    static double somme(int a, double b) {
        System.out.println("1 entiers 1 double");
        return a + b;
    }

    static double somme(double a, double b) {
        System.out.println("2 doubles");
        return a + b;
    }

    static String somme(String s1, String s2) {
        System.out.println("2 chaines");
        return s1 + s2;
    }

    static int somme(int c, int v, int z) {
        System.out.println("3 entiers");
        return c + v + z;
    }

    static int somme(int... v) {
        int s = 0;
        for (int e : v) {
            s += e;
        }
        return s;
    }

    // Méthode récursive
    static int factorial(int n) { // factoriel= 1* 2* … n
        if (n <= 1) { // condition de sortie
            return 1;
        } else {
            return factorial(n - 1) * n;
        }
    }

//  Tableau
//  - Ecrire un méthode qui affiche un tableau d’entier passé en paramètre
//  - Ecrire une méthode qui permet de saisir :
//        - La taille du tableau
//        - Les éléments du tableau
//  - Ecrire une méthode qui calcule le maximum et la moyenne 
//  - Faire un menu qui permet de lancer ces méthodes:Faire un menu qui permet de lancer ces méthodes
    static void afficherTab(int[] t) {
        System.out.print("[ ");
        for (int e : t) {
            System.out.print(e + " ");
        }
        System.out.println("]");

    }
    
    static int[] saisirTab(Scanner scan) {
        System.out.print("Saisir le nombre d'élément");
        int size = scan.nextInt();
        int tab[] = new int[size];
        for (int i = 0; i < tab.length; i++) {
            System.out.print("tab[" + i + "]=");
            tab[i] = scan.nextInt();
        }
        return tab;
    }

    static int maximumTab(int[] tab) {
        int max = tab[0]; // Integer.MIN_VALUE
        for (int elm : tab) {
            if (elm > max) {
                max = elm;
            }
        }
        return max;
    }

    static double moyenneTab(int[] tab) {
        double somme = 0;
        for (int elm : tab) {
            somme += elm;
        }
        return somme / tab.length;
    }

    static void menu(Scanner sc) {
        System.out.println("1-Saisie du tableau");
        System.out.println("2-Afficher le tableau");
        System.out.println("3-Calculer le maximum et la moyenne");
        System.out.println("4-Quitter");

        int[] t = null;
        int choix = 0;
        do {

            System.out.print("choix=");
            choix = sc.nextInt();

            switch (choix) {
            case 1:
                t = saisirTab(sc);
                break;
            case 2:
                if (t != null) {
                    afficherTab(t);
                }
                break;
            case 3:
                if (t != null) {
                    System.out.println("maximum=" + maximumTab(t) + " moyenne=" + moyenneTab(t));
                }
                break;
            case 4:
                System.out.println("Au revoir");
                break;
            default:
                System.out.println("Ce choix n'existe pas");
            }
        } while (choix != 4);

    }

}
