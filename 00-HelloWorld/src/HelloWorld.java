/**
 * Le programme Hello World
 * @author Jehann
 *
 */
public class HelloWorld {
    
    /**
     * Point d'entrée du programme
     * @param args
     */
    public static void main(String[] args) {
        /*
         * Commentaire
         * sur 
         * plusieurs
         */ 
         // Commentaire fin de ligne
        System.out.println("Helloworld");
    }

}
